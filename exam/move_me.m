function B= move_me(A)
T=input('T=');
B = T*ones(size(A));
G=A~=T; 
B(6-sum(G):end) = A(G);
end
