function Q=intquad(n)
T=ones(n);
Q=[T*-1,T*exp(1);T*pi,T];
end